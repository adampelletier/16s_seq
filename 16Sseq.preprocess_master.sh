#!/bin/bash
# @author Slim Fourati (sxf279@case.edu)
# @author Adam Pelletier (anp83@case.edu)



# read input arguments
email="anp83@case.edu"
similarity="97"
acceptedSimilarity=("99" "97" "94" "90")
acceptedLevels=("1" "2" "3" "4" "5" "6" "7","default")
joinpe=false
betad=false
alphad=false
level="default"
while getopts d:e:s:L:jbah option
do
    case "${option}" in
    h) echo "Command: bash mRNA.preprocess_master.sh -d {fastq/directoryfastq} ..."
        echo "FASTQ files must be in the format <sampleID>_R1_001.fastq.gz .\n To use the join paired-end parameters, please supply index files with the associated sample ID"
        echo "argument: d=[d]irectory with raw data (required)"
        echo "          s=[s]imilarity index"
        echo "          j= Launch multiple [j]oin paired-end parameters "
        echo "          e=[e]mail address"
        echo "          b=[b]eta diversity analysis (accross samples)"
        echo "          a=[a]lpha diversity analysis (within samples)"
        echo "          L= Taxonomic [L]evel to summarize data with. 1 being the kingdom, 2 the phylum,..., 6 the genus and 7 the species. Default setting computes levels 1 to 6 "
        echo "          h= print [h]elp"
        exit 1;;
    d) dirFastq=$OPTARG;;
    e) email=$OPTARG;;
    s) similarity=$OPTARG
        if [[ ! "${acceptedSimilarity[@]}" =~ "$similarity" ]]
        then
        echo "Invalid -s argument: choose between ${acceptedSimilarity[@]}"
        exit 1
        fi;;
    j) joinpe=true;;
    b) betad=true;;
    a) alphad=true;;
    L) level=$OPTARG
        if [[ ! "${acceptedLevels[@]}" =~ "$level" ]]
        then
        echo "Invalid -L argument: choose between ${acceptedLevels[@]}"
        exit 1
        fi;;
    \? ) echo "Invalid Option: -$OPTARG" 1>&2 ;;
    esac
done

# test that directory is provided
if [ -z ${dirFastq+x} ]
then
    echo "error...option -d required."
    exit 1
fi

# test that directory contains seq files
suffix="fastq.gz"
nfiles=$(find $dirFastq -name "*_R1*.$suffix" | wc -l)
echo $(find $dirFastq -name "*_R1*.$suffix")

if [ $nfiles -lt 1 ]
then
    echo "error...empty input directory"
    exit 1
fi

# initialize directories
# remove trailing back slash 
dirData=$(echo $dirFastq | sed -r 's|/$||g')
dirData=$(echo $dirData | sed -r 's|/[^/]+$||g')


#1a. generate join-PE-parameters.txt
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: generating qiime parameters file...\n"
    header="#SampleID\tBarcodeSequence\tLinkerPrimerSequence\tReversePrimer"
    header="$header\tDescription"
    echo -e $header > qiime_parameters.txt
    sampleID=$(find $dirFastq -name "*R1*.fastq.gz")
    sampleID=$(echo $sampleID | sed -r 's/R1_001.fastq.gz//g')
    sampleID=( $(echo $sampleID | tr ' ' '\n' | sort | uniq) )
    for sample in ${sampleID[@]}
    do
  # extract SampleID
  sid=$(echo $sample | sed -r "s|.+/([^_]+)_[^/]+$|\\1|g")
  #sid=$(echo $sample | sed -r "s|.+/([^_]+)_[^/]+$|\\1|g")
  # extract foward index
  fwdIndex=$(zcat ${sample}I1_001.fastq.gz | head -n 2 | tail -n 1)
  # extract reverse index
  revIndex=$(zcat ${sample}I2_001.fastq.gz | head -n 2 | tail -n 1)
  echo -e "$sid\tNNNNNNNN\t$fwdIndex\t$revIndex\tNA" >> qiime_parameters.txt
    done
    echo "done"
fi



for i in `seq 1 $nfiles`
do
    if [ ! -d $dirData/sample$i/raw ]
    then
        mkdir -p $dirData/sample$i/raw
    fi
    find $dirFastq -maxdepth 1 -name "*R1_001.fastq.gz" | \
            sed -r "s/R1_/R2_/g" | \
            head -n 1 | xargs -i mv {} ${dirData}/sample${i}/raw
   find $dirFastq -maxdepth 1 -name "*R1_001.fastq.gz" | \
       head -n 1 | xargs -i mv {} ${dirData}/sample${i}/raw
done


# modify preprocessing slurm script
sed -ri "s|^#SBATCH --array=1-.+$|#SBATCH --array=1-${nfiles}|g" \
    16Sseq.preprocess_slurm.sh
sed -ri "s|^dirData=.+|dirData=${dirData}|g" \
    16Sseq.preprocess_slurm.sh
sed -ri "s|^#SBATCH --mail-user=.+$|#SBATCH --mail-user=${email}|g" \
    16Sseq.preprocess_slurm.sh


cmd="sbatch 16Sseq.preprocess_slurm.sh -d $dirData -s $similarity -L $level "
if $joinpe
then
    cmd="$cmd -j"
fi    
if $betad
then 
    cmd="$cmd -b"
fi
if $alphad
then
    cmd="$cmd -a"
fi

slurmid=$(eval $cmd | sed -r 's|Submitted batch job ([0-9]*)|\1|g')


sed -ri "s|^#SBATCH --array=1-.+$|#SBATCH --array=1-${nfiles}|g" \
    16Sseq.betadiv_slurm.sh
sed -ri "s|^dirData=.+|dirData=${dirData}|g" \
    16Sseq.betadiv_slurm.sh
sed -ri "s|^#SBATCH --mail-user=.+$|#SBATCH --mail-user=${email}|g" \
    16Sseq.betadiv_slurm.sh
sed -ri "s|^#SBATCH --depend=afterok:.+$|#SBATCH --depend=afterok:${slurmid}|g" \
    16Sseq.betadiv_slurm.sh

cmd="sbatch 16Sseq.betadiv_slurm.sh -d $dirData -s $similarity -L $level"
if $unweighted
then
    cmd="$cmd -u"
fi
if $betad
then
    cmd="$cmd -b"
fi

if $alphad;then
    cmd="$cmd -a"
fi

slurmid=$(eval $cmd | sed -r 's|Submitted batch job ([0-9]*)|\1|g')




