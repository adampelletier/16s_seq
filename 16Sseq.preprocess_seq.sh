#!/bin/bash

# load modules
module load gcc/6.3.0
module load openmpi/2.0.1
module load qiime/1.9.1
module load gsl/2.3
rdpPath=/mnt/projects/SOM_PATH_RXS745U/bin/rdp_classifier_2.2
rdpPath="$rdpPath/rdp_classifier-2.2.jar"
bin="/mnt/projects/SOM_PATH_RXS745U/bin"
export RDP_JAR_PATH=${rdpPath}

similarity="99"
# read arguments
joinpe=false
qcjoined=false
betad=false
alphad=false
level="default"
while getopts d:s:L:hjba option
do
    case "$option" in
    d) dirData=$OPTARG;;
    s) similarity=$OPTARG;;
    j) joinpe=true;;
    b) betad=true;;
    a) alphad=true;;
    L) level=$OPTARG;;
    \? ) echo "Invalid Option: -$OPTARG" 1>&2 ;;
    esac 
done


#genomeDir=/mnt/projects/SOM_PATH_RXS745U/genome/Greengenes
genomeDir=/mnt/projects/SOM_PATH_RXS745U/genome/SILVA/SILVA_132_QIIME_release

# 1b. lauch multiple_join_paired_ends.py
#joinpe=true
if $joinpe
then
    qcjoined=true
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: joining pair-end files..."
    if [ ! -d ${dirData}/1_joined-fastqs ]
    then
	mkdir ${dirData}/1_joined-fastqs
    fi
    multiple_join_paired_ends.py \
	-i ${dirData} \
	-o ${dirData}/1_joined-fastqs \
	--read1_indicator _R1_ \
	--read2_indicator _R2_ \
	-p join-PE-parameters.txt 2>/dev/null
    # rename assembled files
    sampleID=$(find ${dirData}/1_joined-fastqs -name "fastqjoin.join.fastq")
    sampleID=( $(echo $sampleID | tr ' ' '\n' | sort | uniq) )
    for sample in ${sampleID[@]}
    do
	fqFile=$(echo $sample | \
		     sed -r "s|.+/([^_]+)_[^/]+/fastqjoin.join.fastq|\\1.fastq|g")
	mv ${sample} ${dirData}/1_joined-fastqs/${fqFile}
    done
    # remove unassembled reads
    rm ${dirData}/1_joined-fastqs/*/fastqjoin.un[1-2].fastq
    rmdir ${dirData}/1_joined-fastqs/*_R1_001
    echo "done"
fi

# 1c. quality filter the joined reads

if $qcjoined
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: quality filter joined reads..."
    if [ ! -d ${dirData}/2_quality-check ]
    then
        mkdir ${dirData}/2_quality-check
    fi
    multiple_split_libraries_fastq.py \
	-i ${dirData}/1_joined-fastqs \
	-o ${dirData}/2_quality-check \
	-p split-libraries-parameters.txt 2>/dev/null
    echo "done"
fi

# 1d. truncate the reverse primer
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "${currentDate}: truncate reverse primer..."
    if [ ! -d ${dirData}/3_trunc-rev-primer ]
    then
        mkdir ${dirData}/3_trunc-rev-primer
    fi    
    truncate_reverse_primer.py \
	-f ${dirData}/2_quality-check/seqs.fna \
	-m qiime_parameters.txt \
	-o ${dirData}/3_trunc-rev-primer 2>/dev/null
    echo "done"
fi

# 2a. create openref_rdp_silva.txt
open_ref="openref${similarity}_rdp_silva.txt"
ortext="#Parameters for ${similarity}pct open reference OTU picking with rdp taxonomy assignment\n\n\n# OTU picker parameters"
ortext="${ortext}\npick_otus:similarity\t0.$similarity"
ortext="${ortext}\npick_otus:enable_rev_strand_match\tTrue\n"  #possibly needs to be modified
ortext="${ortext}\n# Taxonomy assignment parameters"
ortext="${ortext}\nassign_taxonomy:reference_seqs_fp\t$genomeDir/rep_set/rep_set_16S_only/${similarity}/silva_132_${similarity}_16S.fna"
ortext="${ortext}\nassign_taxonomy:id_to_taxonomy_fp\t$genomeDir/taxonomy/16S_only/${similarity}/taxonomy_all_levels.txt"
ortext="${ortext}\nassign_taxonomy:assignment_method\trdp"
ortext="${ortext}\nassign_taxonomy:confidence\t\t0.7"
ortext="${ortext}\nassign_taxonomy:rdp_max_memory\t\t1200000"
echo -e $ortext > $open_ref

# 2b.picking OTUs using the open reference strategy
flag=true
errorlog=${dirData}/4-uclust/errorlog.txt
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "${currentDate}: picking otus..."
    if [ ! -d ${dirData}/4-uclust ]
    then
        mkdir ${dirData}/4-uclust
    fi
    pick_open_reference_otus.py \
	-i ${dirData}/3_trunc-rev-primer/seqs_rev_primer_truncated.fna \
	-r ${genomeDir}/rep_set/rep_set_16S_only/${similarity}/silva_132_${similarity}_16S.fna \
	-o ${dirData}/4-uclust \
	-p $open_ref \
	-s 0.10 \
	--prefilter_percent_id 0.0 \
	--suppress_align_and_tree \
	-f 2>$errorlog
    echo "done"
fi


# 3a. identify chimeras
flag=true
errorlog=${dirData}/5-chimera-check/chim-errorlog.txt
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: identify chimeras..."
    if [ ! -d $dirData/5-chimera-check ]
    then
        mkdir $dirData/5-chimera-check
    fi
    identify_chimeric_seqs.py \
    -i $dirData/4-uclust/rep_set.fna \
    -m usearch61 \
    -o ${dirData}/5-chimera-check \
    -r ${genomeDir}/rep_set/rep_set_16S_only/${similarity}/silva_132_${similarity}_16S.fna 2>$errorlog
    echo "done"
fi

# 3b. remove any sequences flagged as chimeras from the BIOM table
flag=true
errorlog=${dirData}/5-chimera-check/errorlog.txt
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: remove chimeras..."
    filter_otus_from_otu_table.py \
    -i ${dirData}/4-uclust/otu_table_mc2_w_tax.biom \
    -o ${dirData}/5-chimera-check/otu-table-mc2_wo_chim.biom \
    -e ${dirData}/5-chimera-check/chimeras.txt 2>$errorlog
    echo "done"
fi

# 4a. align rep_set.fna against a pre-aligned reference database
flag=true
errorlog=${dirData}/6-pynast-aligned/errorlog.txt
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: align representative reads to reference db..."
    if [ ! -d ${dirData}/6-pynast-aligned ]
    then
        mkdir ${dirData}/6-pynast-aligned
    fi
    align_seqs.py \
    -i ${dirData}/4-uclust/rep_set.fna \
    -o ${dirData}/6-pynast-aligned \
    -t ${genomeDir}/rep_set_aligned/${similarity}/${similarity}_alignment.fna \
    --alignment_method pynast \
    --pairwise_alignment_method uclust \
    --min_percent_id 70.0 2>$errorlog
    echo "done"
fi

# 4b. remove gaps from the aligned rep sets
flag=true
gaplog=${dirData}/6-pynast-aligned/errorlog.txt
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: remove gaps from reps alignment..."
    filter_alignment.py \
    -i ${dirData}/6-pynast-aligned/rep_set_aligned.fasta \
    -o ${dirData}/6-pynast-aligned \
    --suppress_lane_mask_filter 2>$gaplog
    echo "done"
fi

# 4c. add metadata to the BIOM table
errorlog=${dirData}/6-pynast-aligned/errorlog.txt
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: add metadata to BIOM table..."
    biom add-metadata \
    -i ${dirData}/5-chimera-check/otu-table-mc2_wo_chim.biom \
    -o ${dirData}/6-pynast-aligned/otu-table.biom \
    -m qiime_parameters.txt 2>$errorlog #/dev/null
    echo "done"
fi

# 4d. summarize the BIOM table to assess the number of sequences/OTUs per sample
#     and store the output in a text file
flag=true
summary_error_log=${dirData}/6-pynast-aligned/summary_errorlog.txt
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: summarize biom table..."
    biom summarize-table \
     -i ${dirData}/6-pynast-aligned/otu-table.biom \
     -o ${dirData}/6-pynast-aligned/otu-table-summary.txt 2>$summary_error_log
    echo "done"
fi

# 4e. absolute counts
flag=true
errorlog=${dirData}/7-tax_mapping/counts_errorlog.txt

if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: count reads..."
    if [ ! -d ${dirData}/7-tax_mapping ]
    then
        mkdir ${dirData}/7-tax_mapping
    fi
    if [ "$level" == "7" ]
    then
        summarize_taxa.py \
        -i ${dirData}/6-pynast-aligned/otu-table.biom \
        -o ${dirData}/7-tax_mapping \
        -L $level
        -a 2>$errorlog #/dev/null
    else 
        summarize_taxa.py \
        -i ${dirData}/6-pynast-aligned/otu-table.biom \
        -o ${dirData}/7-tax_mapping \
        -a 2>$errorlog #/dev/null
    fi
    echo "done"
fi

# 5. filter rep set fasta file to match the OTU IDs in your filtered OTU table
if $alphad || $betad
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: matching OTU to representative fasta..."
    filter_fasta.py \
    -f ${dirData}/6-pynast-aligned/rep_set_aligned_pfiltered.fasta \
    -o ${dirData}/6-pynast-aligned/biom_filtered_seqs.fasta \
    -b ${dirData}/6-pynast-aligned/otu-table.biom 2>/dev/null
    echo "done"
fi

# 6. make new phylogeny with final set of OTUs
if $alphad || $betad
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: making phylogeny..."
    if [ ! -d $dirData/8-phylogeny ]
    then
        mkdir $dirData/8-phylogeny
    fi
    make_phylogeny.py \
    -i ${dirData}/6-pynast-aligned/biom_filtered_seqs.fasta \
    -o ${dirData}/8-phylogeny/rep_phylo.tre \
    --tree_method fasttree 2>/dev/null
    echo "done"
fi

# 7a. run diversity analysis
if $alphad
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: running alpha diversity..."
    if [ ! -d ${dirData}/9-diversity ]
    then
        mkdir ${dirData}/9-diversity
    fi
    # if [ $level == "default" ]; then
    #     infile_adir="otu-table_L6.biom"
    # else
    #     infile_adir="otu-table_L${level}.biom"
    # fi

    alpha_diversity.py \
    -i ${dirData}/6-pynast-aligned/otu-table.biom \
    -t ${dirData}/8-phylogeny/rep_phylo.tre \
    -m PD_whole_tree,shannon,simpson,chao1,observed_otus \
    -o ${dirData}/9-diversity/alpha_div.txt 2>/dev/null
    echo "done"
fi



