#!/bin/bash

# load modules
module load gcc/6.3.0
module load openmpi/2.0.1
module load qiime/1.9.1
module load gsl/2.3
rdpPath=/mnt/projects/SOM_PATH_RXS745U/bin/rdp_classifier_2.2
rdpPath="$rdpPath/rdp_classifier-2.2.jar"
export RDP_JAR_PATH=${rdpPath}

# read input arguments
#dirData="/scratch/users/sxf279/20180214_16S"
genomeDir=/mnt/projects/SOM_PATH_RXS745U/genome/SILVA/SILVA_132_QIIME_release
similarity="99"
level="2"
betad=false
alphad=false

# read arguments

while getopts d:s:L:hba option
do
    case "$option" in
    d) dirData=$OPTARG;;
    s) similarity=$OPTARG;;
    L) level=$OPTARG;;
    b) betad=true;;
    a) alphad=true;;
    \? ) echo "Invalid Option: -$OPTARG" 1>&2 ;;
    esac
done


# 7b. collate OTU tables
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: collating OTU tables..."
    sampleID=$(find $dirData -name "otu-table_L${level}.biom")
    sampleID=$(echo $sampleID | sed -r 's/ /,/g')
    merge_otu_tables.py \
    -i $sampleID \
    -o $dirData/merged_otu_table_L${level}.biom
    sampleID=$(find $dirData -name "otu-table.biom")
    sampleID=$(echo $sampleID | sed -r 's/ /,/g')
    merge_otu_tables.py \
    -i $sampleID \
    -o $dirData/merged_otu_table.biom
    #-o $dirData/merged_otu_table_L${level}.biom
    echo "done"
fi

# 7c. Combine raw pre-alignment read count logs 
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: collating rawcount logs..."
    sampleID=$(find $dirData -name "split_library_log.txt") 
    echo -e "\n" > ${dirData}/raw_counts_log.txt
    for sample in ${sampleID[@]}
    do
        output=$(tail -n+13 $sample)
        output="${output}\n"
        echo -e $output >> ${dirData}/raw_counts_log.txt
    done
    echo "done"
fi


# 8. make new phylogeny with final set of OTUs
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: making phylogeny..."
    if [ ! -d $dirData/trees ]
    then
        mkdir $dirData/trees
    fi
    # copy rep trees into a central directory
    sampleID=$(find $dirData -name "rep_phylo.tre")
    sampleID=( $(echo $sampleID | tr ' ' '\n' | sort | uniq) )
    for sample in ${sampleID[@]}
    do
    output=$(echo $sample | sed -r 's|.+/([^/]+)/raw/8-phylogeny.+|\1|g')
    output="${dirData}/trees/${output}.rep_phylo.tre"
    cp $sample $output
    done
    consensus_tree.py \
    -i $dirData/trees \
    -o $dirData/consensus_tree.tre 2>/dev/null
    echo "done"
fi

# 9. convert merged biom table
flag=true
if $flag;then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "currentDate: converting BIOM table to text format..."
    biom convert \
    -i $dirData/merged_otu_table_L${level}.biom \
    -o $dirData/merged_otu_table_L${level}.txt \
    --to-tsv 2>/dev/null
    echo "done"
fi


# 10. combine alpha diversity tables
if $alphad;then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "currentDate: conbining alpha diversity tables..."
    sampleID=$(find $dirData -name "alpha_div.txt")
    firstfile=$(echo $sampleID | tr " " "\n" | sed -n '1p')
    header=$(head -n1 $firstfile) 
    header="$header\n"
    echo -e $header > ${dirData}/alpha_div_merged.txt
    for sample in ${sampleID[@]}
    do
        output=$(tail -n+2 $sample)
        output="${output}\n"
        echo -e $output >> ${dirData}/alpha_div_merged.txt
    done
    echo "done"
fi

# 10. run beta diversity

if $betad
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: running beta diversity..."
    if [ ! -d $dirData/beta_div ]
    then
        mkdir $dirData/beta_div
    fi
    beta_diversity.py \
    -i $dirData/merged_otu_table.biom \
    -o $dirData/beta_div/ \
    -t $genomeDir/trees/${similarity}/${similarity}_otus.tre
    echo "done"
fi
